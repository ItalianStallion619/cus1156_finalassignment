from app.api import bp
from flask import jsonify, request, url_for, render_template
from app.models import Temperature, Weight, Currency, BillingLog
from app import db
from sqlalchemy import func
from app.api.errors import bad_request
from jinja2 import Environment, PackageLoader, select_autoescape

env = Environment(
    loader=PackageLoader('app', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)

billing_Log = BillingLog(num_of_requests_temp=0, num_of_requests_weight=0, num_of_requests_currency=0)

@bp.route('/temperature/c2f/<int:temp>', methods=['GET'])
def convert_c_to_f(temp):
    temperature = Temperature()
    billingLog = BillingLog(ip_address=request.remote_addr, request_type="Temperature")
    billing_Log.num_of_requests_temp +=  1
    billingLog.num_of_requests_temp = billing_Log.num_of_requests_temp
    db.session.add(billingLog)
    db.session.commit()
    return jsonify(temperature.c_to_f(temp))

@bp.route('/temperature/f2c/<int:temp>', methods=['GET'])
def convert_f_to_c(temp):
    temperature = Temperature()
    billingLog = BillingLog(ip_address=request.remote_addr, request_type="Temperature")
    billing_Log.num_of_requests_temp += 1
    billingLog.num_of_requests_temp = billing_Log.num_of_requests_temp
    db.session.add(billingLog)
    db.session.commit()
    return jsonify(temperature.f_to_c(temp))

@bp.route('/weight/k2p/<int:weig>', methods=['GET'])
def convert_kg_to_lb(weig):
    weight = Weight()
    billingLog = BillingLog(ip_address=request.remote_addr, request_type="Weight")
    billing_Log.num_of_requests_weight += 1
    billingLog.num_of_requests_weight = billing_Log.num_of_requests_weight
    db.session.add(billingLog)
    db.session.commit()
    return jsonify(weight.kg_to_lb(weig))

@bp.route('/weight/p2k/<int:weig>', methods=['GET'])
def convert_lb_to_kg(weig):
    weight = Weight()
    billingLog = BillingLog(ip_address=request.remote_addr, request_type="Weight")
    billing_Log.num_of_requests_weight += 1
    billingLog.num_of_requests_weight = billing_Log.num_of_requests_weight
    db.session.add(billingLog)
    db.session.commit()
    return jsonify(weight.lb_to_kg(weig))

@bp.route('/currency/usd2eur/<int:value>', methods=['GET'])
def convert_usd_to_euro(value):
    currency = Currency()
    billingLog = BillingLog(ip_address=request.remote_addr, request_type="Currency")
    billing_Log.num_of_requests_currency += 1
    billingLog.num_of_requests_currency = billing_Log.num_of_requests_currency
    db.session.add(billingLog)
    db.session.commit()
    return jsonify(currency.usd_to_eur(value))

@bp.route('/currency/eur2usd/<int:value>', methods=['GET'])
def convert_euro_to_usd(value):
    currency = Currency()
    billingLog = BillingLog(ip_address=request.remote_addr, request_type="Currency")
    billing_Log.num_of_requests_currency += 1
    billingLog.num_of_requests_currency = billing_Log.num_of_requests_currency
    db.session.add(billingLog)
    db.session.commit()
    return jsonify(currency.eur_to_usd(value))


@bp.route('/billing/', methods=['GET'])
def billing():
    ip_address = db.session.query(BillingLog.ip_address).first()
    temp_records = db.session.query(func.sum(BillingLog.num_of_requests_temp)).scalar()
    weight_records = db.session.query(func.sum(BillingLog.num_of_requests_weight)).scalar()
    currency_records = db.session.query(func.sum(BillingLog.num_of_requests_currency)).scalar()
    total_records = temp_records + weight_records + currency_records
    return render_template('billing.html', title='Home', temp_records=temp_records, weight_records=weight_records, currency_records=currency_records, total_records=total_records, ip_address=ip_address)