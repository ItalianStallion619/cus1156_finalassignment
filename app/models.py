#from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
from app import db, login, app
from sqlalchemy import *
from flask_login import UserMixin
from flask import url_for
import base64
from datetime import datetime, timedelta
import os

class PaginatedAPIMixin(object):
    @staticmethod
    def to_collection_dict(query, page, per_page, endpoint, **kwargs):
        resources = query.paginate(page, per_page, False)
        data = {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page, **kwargs),
                'next': url_for(endpoint, page=page + 1, per_page=per_page, **kwargs) if resources.has_next else None,
                'prev': url_for(endpoint, page=page - 1, per_page=per_page, **kwargs) if resources.has_prev else None
            }
        }
        return data

class Temperature():
        
    def f_to_c(self, f):
        c = (f - 32) * 5/9
        return {
            'conversionType': "Temperature",
            'from': "Fahrenhight",
            'fromValue': f,
            'to': "Celsius",
            'toValue': c
        }
    def c_to_f(self, c):
        f = (c * 9/5) + 32
        return {
            'conversionType': "Temperature",
            'from': "Celsius",
            'fromValue': c,
            'to': "Fahrenhight",
            'toValue': f
        }

class Weight():

    def lb_to_kg(self, lb):
        kg = lb / 2.205
        return {
            'conversionType': "Weight",
            'from': "Pounds",
            'fromValue': lb,
            'to': "Kilograms",
            'toValue': kg
        }
    def kg_to_lb(self, kg):
        lb = kg * 2.205
        return {
            'conversionType': "Weight",
            'from': "Kilograms",
            'fromValue': kg,
            'to': "Pounds",
            'toValue': lb
        }

class Currency():

    def usd_to_eur(self, usd):
        eur = usd * .88
        return {
            'conversionType': "Currency",
            'from': "USD",
            'fromValue': usd,
            'to': "EUR",
            'toValue': eur
        }
    def eur_to_usd(self, eur):
        usd = eur * 1.13
        return {
            'conversionType': "Currency",
            'from': "Euro",
            'fromValue': eur,
            'to': "USD",
            'toValue': usd
        }

class BillingLog(db.Model):
    ip_address = db.Column(db.String(20))
    request_type = db.Column(db.String(12))
    date_created = db.Column(db.DateTime())
    total_num_of_requests = db.Column(db.Integer, default=0)
    num_of_requests_temp = db.Column(db.Integer, default=0)
    num_of_requests_weight = db.Column(db.Integer, default=0)
    num_of_requests_currency = db.Column(db.Integer, default=0)
    id = db.Column(db.Integer, primary_key=true)
    #def insert(self, BillingLog):
        #db.session.add(billingLog)
        #db.session.commit()